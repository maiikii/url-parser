/**
 *  Parses the given URL into its different components.
 *
 *  TODO: Implement this function.
 *  NOTE: You may implement additional functions as you need, as long as this
 *    function behaves as specified in the instructions. Have fun! :)
 **/
function parse(url) {
	var parser = document.createElement("A");
	parser.href = url;
	var scheme = parser.protocol;
	var search = parser.search;
	var hash = parser.hash;

	scheme = scheme.substring(0, scheme.length-1);
	search = search.substring(1, search.length);
	hash = hash.substring(1, hash.length);

	return expected = {
	    scheme: scheme === "" ? null : scheme,
	    authority: {
	      username: parser.username === "" ? null : parser.username,
	      password: parser.password === "" ? null : encode(parser.password),
	      host: parser.hostname === "" ? null : parser.hostname,
	      port: checkPort(scheme, parser.port)
	    },
	    path: checkPath(parser.hostname, url) ? encode(space(parser.pathname)) : "",
	    query: search === "" ? null : getSearch(search),
	    fragment: hash === "" ? null : hash
	};
}

function checkPort(scheme, port) {
	var arr = ["http", "https", "ssh", "ftp"]
	if(scheme === "file" || arr.indexOf(scheme) === -1) {
		return null;
	}

	if(port === '') {
		if(scheme === "https") {
			return "443";
		}else if(scheme === "ftp") {
			return "21";
		}else if(scheme === "ssh") {
			return "22";
		}else {
			return "80";
		}
	}
	return port;
}

function checkPath(host, url) {
	var ndx = url.indexOf(host) + host.length;
	url = url.substring(ndx, url.length);
	if(url.indexOf("/") === -1) {
		return false;
	}
	return true;
} 

function getSearch(search) {
	var obj = {};
	var amp;
	var eql;

	if(search.search("&") < 0) {
		eql = search.split("=");
		obj[eql[0]] = encode(space(eql[1]));
		return obj;
	}

	amp = search.split("&");
	for(var i = 0; i < amp.length; i++) {
		eql = amp[i].split("=");
		obj[eql[0]] = encode(space(eql[1]));
	}
	return obj;
}

function space(str) {
	if(str.search("%20") < 0) {
		return str;
	}

	var percent = str.split("%20");
	var phrase = percent[0] + " ";
	for(var i = 1; i < percent.length; i++) {
		phrase = phrase + percent[i];
		if(i < percent.length - 1) {
			phrase += " ";
		}
	}
	return phrase;
}

function encode(str) {
	var code, num, ndx;
	var phrase = "";

	if(str.indexOf("%") === -1) {
		return str;
	}

	while(str.indexOf("%") > -1) {
		ndx = str.indexOf("%");
		phrase += str.substring(0, ndx);
		num = str.substring(++ndx, ndx + 2);
		code = parseInt(num, 16);
		phrase += String.fromCharCode(code);
		ndx += 2;
		str = str.substring(ndx, str.length);
	}
	return phrase + str;
}